# JFLEG 

> This repository displays the public sets from JFLEG. Both sets are merged.

# Description

We present a new parallel corpus, JHU FLuency-Extended GUG corpus (JFLEG) for developing and evaluating grammatical error correction (GEC). Unlike other corpora, it represents a broad range of language proficiency levels and uses holistic fluency edits to not only correct grammatical errors but also make the original text more native sounding. We describe the types of corrections made and benchmark four leading GEC systems on this corpus, identifying specific areas in which they do well and how they can improve. JFLEG fulfills the need for a new gold standard to properly assess the current state of GEC.

This decription is extracted from the [scientific paper presenting JFLEG](https://aclanthology.org/E17-2037/).

# Source

The dataset can be downloaded [here](https://github.com/keisks/jfleg), or can be directly found in the directory [source](#source) of this repository.

To merge side by side the source and the references into a csv file for the test set and one for the dev set: `python3 0_merge.py`
To merge the two sets (test and dev) into one: `cat source/test.csv source/dev.csv > source/full_text.csv`

All files are in the [source](#source) folder.

# Set

We are interested in a detokenized merged file.

To detokenize the sentences: `python3 1_detokenize.py`

This file is located in [set/full_set.csv](#set/full_set.csv). It consists of 1,501 sentences with their four expected sentences from four different annotators.

# References

@InProceedings{napoles-sakaguchi-tetreault:2017:EACLshort,
  author    = {Napoles, Courtney  and  Sakaguchi, Keisuke  and  Tetreault, Joel},
  title     = {JFLEG: A Fluency Corpus and Benchmark for Grammatical Error Correction},
  booktitle = {Proceedings of the 15th Conference of the European Chapter of the Association for Computational Linguistics: Volume 2, Short Papers},
  month     = {April},
  year      = {2017},
  address   = {Valencia, Spain},
  publisher = {Association for Computational Linguistics},
  pages     = {229--234},
  url       = {http://www.aclweb.org/anthology/E17-2037}
}

@InProceedings{heilman-EtAl:2014:P14-2,
  author    = {Heilman, Michael  and  Cahill, Aoife  and  Madnani, Nitin  and  Lopez, Melissa  and  Mulholland, Matthew  and  Tetreault, Joel},
  title     = {Predicting Grammaticality on an Ordinal Scale},
  booktitle = {Proceedings of the 52nd Annual Meeting of the Association for Computational Linguistics (Volume 2: Short Papers)},
  month     = {June},
  year      = {2014},
  address   = {Baltimore, Maryland},
  publisher = {Association for Computational Linguistics},
  pages     = {174--180},
  url       = {http://www.aclweb.org/anthology/P14-2029}
}