import pandas as pd
import argparse

def get_lines(file):
    with open(file, 'r') as f:
        lines = f.readlines()
        f.close()
    for i, line in enumerate(lines):
        lines[i] = line[:-1]
    return lines

def save_csv(df, type):
    return df.to_csv(f'0_sets/{type}.csv', index=False)

def fill_csv(type):
    lines_source = get_lines(f'source/{type}/{type}.source.txt')
    lines_ref0 = get_lines(f'source/{type}/{type}.ref0.txt')
    lines_ref1 = get_lines(f'source/{type}/{type}.ref1.txt')
    lines_ref2 = get_lines(f'source/{type}/{type}.ref2.txt')
    lines_ref3 = get_lines(f'source/{type}/{type}.ref3.txt')

    matrix = [[a, b, c, d, e] for a, b, c, d, e in zip(lines_source, lines_ref0, lines_ref1, lines_ref2, lines_ref3)]
    df = pd.DataFrame(matrix, columns=['Original sentence', 'Expected sentence (Annotation 0)', 'Expected sentence (Annotation 1)', 'Expected sentence (Annotation 2)', 'Expected sentence (Annotation 3)'])
    return df

def main(args):
    df_test = fill_csv(args.test)
    save_csv(df_test, args.test)

    df_dev = fill_csv(args.dev)
    save_csv(df_dev, args.dev)

if __name__ == "__main__":
    # Define and parse program input
    parser = argparse.ArgumentParser()
    parser.add_argument("--test", help="test", default="test")
    parser.add_argument("--dev", help="dev", default="dev")
    args = parser.parse_args()

    main(args)
