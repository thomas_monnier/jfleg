import pandas as pd
import argparse
from nltk.tokenize.treebank import TreebankWordDetokenizer
from zmq import device

def rules(sentence):
  """
  This function detokenizes quotes and hyphens.
  Input: str
  Example: 'I told you : "Why do you think I'm a ten-year-old guy ?"
  """
  return sentence.replace(' ?', '?').replace(' !', '!').replace(' ,', ',').replace(' .', '.').replace(' :', ':').replace(' ;', ';')

def nltk_detokenization(sentence):
  """
  This functions detokenizes all ponctuation marks, including paranthesis.
  Input: bag of words. 
  Example: ["I", "'ve", "many", "dogs", "."]
  """
  return TreebankWordDetokenizer().detokenize(sentence)

def detokenize(df):
    for ind, row in df.iterrows():
        row[0] = rules(row[0])
        original = nltk_detokenization(row[0].split())
        row[0] = original
        for i in range(df.shape[1]//2):
            row[2*i+1] = rules(row[2*i+1])
            expected = nltk_detokenization(row[2*i+1].split())
            row[2*i+1] = expected
    return df

def save_csv(df, output):
    return df.to_csv(output, index=False)

def main(set):
  df = pd.read_csv(set["input"])
  df = detokenize(df)
  save_csv(df, set["output"])

if __name__ == "__main__":
  dev = {"input": "0_sets/dev.csv", "output": "1_detokenized_sets/dev.csv"}
  test = {"input": "0_sets/test.csv", "output": "1_detokenized_sets/test.csv"}
  full_set = {"input": "0_sets/full_set.csv", "output": "1_detokenized_sets/full_set.csv"}

  sets = [dev, test, full_set]
  for set in sets:
    main(set)